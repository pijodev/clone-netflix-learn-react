# About

Ce projet est un projet d'entrainement. Je m'en sers pour apprendre les principes de base de React : composants, routing, template jsx, hooks, state etc.

**Disclaimer :** 
Ce projet suit pas à pas le tutoriel de Karl Hawden, disponible [ici](https://youtu.be/x_EEwGe-a9o).

A cet état du projet, j'ai réalisé à peu près la moitié du tutoriel.

Ce projet n'avait pas vocation à être publié sur Gitlab ou Github en raison de son caractère de support d'apprentissage, d'où le fait qu'il n'y a eu aucun petit commit avant le tout 1er. Mais puisqu'il semble que ça intéresse quand même, je décide de le poster. 

Il n'est pas vraiment différent du tutoriel de Karl, si ce n'est mes annotation personnelles qui résument ce que j'ai compris. Donc si quelqu'un cherche le projet parfaitement bien exécuté, il faut cloner directement celui de Karl ! ( [Karl Repo](https://github.com/karlhadwen/netflix) )

Au moment du commit initial, nous avons déjà vu principalement :
- La structure de base
- La création d'un élément réutilisé fréquemment pour affichage des films (Jumbotron)
- Styles, propriétés
- Les children
- Styled Comp
- Routes et routes protégées
- Pages
- Ajout de Firebase et log in
- Helpers
- Barre de navigation
- Custom hooks

Nous sommes arrêtés à l'étape : 
- Browser container


**Note sur Firebase :** 

Attention, le Firebase associé à ce projet est inactif, il n'est donc pas possible d'enregistrer des utilisateurs maintenant. (Et même si c'était possible, on ne peut pas regarder les séries avec :) )


# Utiliser le clone

Si vous souhaitez voir à quoi ressemble le clone actuellement, *il vous faut npm (6.x)*.

Cloner le projet :

`git clone git@gitlab.com:pijodev/clone-netflix-learn-react.git`

Il faudra éventuellement installer les dépendances :

`npm install`

Lancer le serveur :

`npm start`

Le client est accessible à l'adresse :

`localhost:3000`
