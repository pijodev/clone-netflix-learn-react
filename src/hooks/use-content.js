import { useEffect, useState, useContext } from "react";
import { FirebaseContext } from "../context/firebase";

export default function useContent(target) {
    const [content, setContent] = useState([]);
    const { firebase } = useContext(FirebaseContext);


    useEffect(() => {
        firebase
            .firestore()
            .collection(target) // = film / series
            .get()
            // snapshot : toute la collection de films ou séries
            .then((snapshot) => {
                // destructuration du snapshot en une const allContent contenant infos de base + un id
                const allContent = snapshot.docs.map((contentObj) => ({
                    ...contentObj.data(),
                    docId: contentObj.id, // spécifique à React
                }));

                setContent(allContent);
            })
            .catch((error) => { console.log(error.message) })


    }, [])

    // on assigne à la cible (films ou série) la liste du contenu !
    // renvoie par ex : { films : {listeFilms} }
    return { [target]: content} ;
}