// Ce fichier va permettre de savoir l'état de connexion d'un utilisateur dans l'application.

import { useState, useEffect, useContext } from 'react';
import { FirebaseContext } from '../context/firebase';

export default function useAuthListener() {
    // Pour déterminer si un user est connecté, il faut le stocker !

    // Vérifions d'abord le storage local. 
    // Pour ça, il faut prendre soin de stocker l'état utilisateur sur toute action type login, logout.
    const [user, setUser] = useState(
        JSON.parse(localStorage.getItem('authUser')));

    const { firebase } = useContext(FirebaseContext);

    useEffect( () => {
        const listener = firebase.auth()
        .onAuthStateChanged( (authUser) => { 
            if (authUser) {
                localStorage.setItem('authUser', JSON.stringify(authUser));
                setUser(authUser);
            } else {
                localStorage.removeItem('authUser');
                setUser(null);
            }
        });

        return () => listener();
    }, []);
    return { user }
}

// ATTENTION, les listener peuvent affecter le comportement de React ! il faut en savoir +.