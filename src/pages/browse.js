import React from 'react';
import { useContent } from '../hooks';
import {selectionFilter} from '../utils';
import { BrowseContainer } from '../containers/browser';

export default function Browse() {

    // show films / series 
    // display as slides
    // il faut afficher tout ça dans le browser container ( à créer )

    const { series } = useContent('series');
    const { films } = useContent('films');

    const slides = selectionFilter({series, films});

    return (
        <>
            <BrowseContainer></BrowseContainer>
        </>

    );
}