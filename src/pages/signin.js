
import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom'; // pratique pour renvoyer vers différentes pages sur 1 action
// exemple : l'utilisateur s'inscrit avec succès : on le propulse alors vers une page par exemple browse.
import { HeaderContainer } from '../containers/header';
import { FooterContainer } from '../containers/footer';
import { Form } from '../components/';
import { FirebaseContext } from '../context/firebase';
import * as ROUTES from '../constants/routes';

export default function Signin() {
    // on veut pouvoir contrôler les éléments importés
    const [emailAdress, setEmailAdress] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const { firebase } = useContext(FirebaseContext);
    const history = useHistory();

    // check form input elements are valid
    const isInvalid = password === '' || emailAdress === '';

    // email & password
    const handleSignin = (event) => {
        event.preventDefault();

        // firebase here !
        firebase
            .auth()
            .signInWithEmailAndPassword(emailAdress, password)
            // Je pense que history remplace le fait de créer et stocker en état le user pour le faire
            // voyager de page en page, comme dans app.js où on crée un utilisateur null au début de la session.
            .then(() => history.push(ROUTES.BROWSE) )
            .catch((error) => {
                    setEmailAdress('');
                    setPassword('');
                    setError(error.message);
                });
    };

    return (
        <>
            <HeaderContainer>

                <Form>
                    <Form.Title> Connectez-vous !</Form.Title>

                    {error && <Form.Error>{error}</Form.Error>}

                    <Form.Base onSubmit={handleSignin} method="POST">
                        <Form.Input
                            placeholer="Votre email"
                            value={emailAdress}
                            onChange={({ target }) => setEmailAdress(target.value)}
                        />
                        <Form.Input
                            placeholer="Password"
                            type="password"
                            autoComplete="off"
                            value={password}
                            onChange={({ target }) => setPassword(target.value)}
                        />

                        <Form.Submit disabled={isInvalid} type="submit"> Connexion </Form.Submit>
                    </Form.Base>

                    <Form.Text>Nouveau chez Netflix ?
                        <Form.Link to={ROUTES.SIGN_UP}>Inscrivez-vous maintenant ;) </Form.Link>
                    </Form.Text>
                    <Form.TextSmall>Page sécurisée contre les robots via Noodle reCarpaccio. #c'estFaux</Form.TextSmall>

                </Form>


            </HeaderContainer>



            <FooterContainer />
        </>

    );
}