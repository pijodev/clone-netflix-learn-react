
import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { HeaderContainer } from '../containers/header';
import { Form } from '../components';
import { FirebaseContext } from '../context/firebase';
import * as ROUTES from '../constants/routes';
import { FooterContainer } from '../containers/footer';


export default function Signup() {

    // history 
    // firebase
    // state : email, password, error
    // isInvalid
    // handleSignup + prevent defualt 



    // HeaderContainer
    // Form
    // Signup title + 3 champs + blabla

    const history = useHistory();
    const { firebase } = useContext(FirebaseContext);
    const [fullName, setFullName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');

    const isInvalid = password === '' || email === '';

    const handleSignup = (event) => {
        event.preventDefault();

        // todo le reste
        firebase
            .auth()
            .createUserWithEmailAndPassword(email, password)
            .then(
                (result) => {
                    result.user.updateProfile({
                        displayName: fullName,
                        photoURL: Math.floor(Math.random() * 5 + 1,)
                    })
                        .then(
                            () => history.push(ROUTES.BROWSE)
                        )
                }
            )
            .catch((error) => {
                setEmail('');
                setPassword('');
                setError(error.message);
            });

    };


    return (
        <>
            <HeaderContainer>
                <Form>
                    <Form.Base onSubmit={handleSignup} method="POST">
                        <Form.Title>Inscrivez-vous à Netflix</Form.Title>
                        {error && <Form.Error>{error}</Form.Error>}
                        <Form.Input
                            value={fullName}
                            onChange={({ target }) => setFullName(target.value)}
                            placeholder="Votre nom complet" />
                        <Form.Input
                            value={email}
                            onChange={({ target }) => setEmail(target.value)}
                            placeholder="Votre email" />
                        <Form.Input
                            value={password}
                            type="password"
                            autoComplete="off"
                            onChange={({ target }) => setPassword(target.value)}
                            placeholder="Mot de passe" />
                        <Form.TextSmall>En vous inscrivant, vous acceptez les CGU.</Form.TextSmall>
                        <Form.Submit disabled={isInvalid} > Je m'inscris ! </Form.Submit>

                        <Form.Text>Déjà inscrit ?
                            <Form.Link to={ROUTES.SIGN_IN}> Alors c'est par ici.</Form.Link>
                        </Form.Text>

                    </Form.Base>
                </Form>
            </HeaderContainer>

            <FooterContainer></FooterContainer>
        </>

    );
}