import React from 'react';
import { JumbotronContainer } from '../containers/jumbotron';
import { FooterContainer } from '../containers/footer';
import { FaqsContainer } from '../containers/faqs';
import { HeaderContainer } from '../containers/header';
import { Feature, OptForm } from '../components';

export default function Home() {
    return (

        <>
            <HeaderContainer>
                
                <Feature>
                    <Feature.Title>
                        Films & séries en illimité et bien plus.
                    </Feature.Title>
                    <Feature.SubTitle>
                        Regardez de n'importe où. Annulez à tout moment.
                    </Feature.SubTitle>

                    <OptForm>
                        <OptForm.Input placeholder="Votre email" />
                        <OptForm.Button > Essayez Netflix ! </OptForm.Button>
                        <OptForm.Text > Bla bla bla rejoignez nous 😇 </OptForm.Text>
                    </OptForm>   
                </Feature>


            </HeaderContainer>

            <JumbotronContainer />
            <FaqsContainer />
            <FooterContainer />


        </>


    );
}