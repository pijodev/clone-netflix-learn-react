// Composant réutilisable dans tout projet 

// A pour but de maintenir l'état de "connexion" de l'utilisateur.
// On veut garder la trace de si l'utilisateur est log in ou log out.

// Fonctionnalités : 
// Lorsque l'utilisateur est connecté, le rediriger à la browse page.
// Routes protégées : browse page.


// On suit les recommandations de React Router

import React from 'react';
import { Route, Redirect } from 'react-router-dom';

// user : utilisateur connecté
// loggedInPath : "si l'utilisateur est connecté, le rediriger vers 'loggedInPath'"
// children : "si l'utilisateur n'est pas connecté, render 'children' (probablement signin ou signup)"
// ...rest : reste des props
export function IsUserRedirect({user, loggedInPath, children, ...rest}) {
    return (
        <Route {...rest } render={ () => {
            if (!user) return children;
            if (user) return <Redirect to={{pathname: loggedInPath}} />;
            return null;
        }}
        />
    )
}

// state from location : preserve history, just to know
export function ProtectedRoute({ user, signinPath, children, ...rest }) {
    return (
        <Route 
            {...rest}
            render={ ({location}) => { 
                    if (user) return children;
                if (!user) return <Redirect to={{ pathname: signinPath, state: {from : location} }} />;
                    return null;
                }
            }
        />
    )
}