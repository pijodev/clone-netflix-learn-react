import { Home, Browse, Signin, Signup } from "./pages";
import * as ROUTES from "./constants/routes";
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import React from 'react';
import { IsUserRedirect, ProtectedRoute } from './helpers/routes';
import { useAuthListener } from "./hooks";





export default function App() {
  const { user } = useAuthListener();
  return (
    <>
      <Router>
        <Switch>
          {/* Routes subissant une redirection si connecté : 
          - HOME
          - SIGNIN
          - SIGNUP
        */}
          <IsUserRedirect
            user={user}
            loggedInPath={ROUTES.BROWSE}
            exact
            path={ROUTES.HOME}>
            <Home />
          </IsUserRedirect>

          <IsUserRedirect user={user} loggedInPath={ROUTES.BROWSE} path={ROUTES.SIGN_IN} exact>
            <Signin />
          </IsUserRedirect>

          <IsUserRedirect user={user} loggedInPath={ROUTES.BROWSE} path={ROUTES.SIGN_UP} exact>
            <Signup />
          </IsUserRedirect>

          {/* Routes protégées : 
          - BROWSE
        */}
          <ProtectedRoute user={user} path={ROUTES.BROWSE} signinPath={ROUTES.SIGN_IN}>
            <Browse />
          </ProtectedRoute>





        </Switch>
      </Router>
    </>
  );
}

