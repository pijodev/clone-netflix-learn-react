import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import { GlobalStyles } from './global-styles';
import { FirebaseContext } from './context/firebase';
import { firebase } from './lib/firebase.prod'

// La notation <> <Another /><Element /> </> est une notation pour les éléments ADJACENTS.
// Ca permet d'afficher le composant de style maison "GlobalStyles" et de l'appliquer à App.
ReactDOM.render(
  <>
    <FirebaseContext.Provider value = {{ firebase }}>
      <GlobalStyles />
      <App />
    </FirebaseContext.Provider>

  </>,
  document.getElementById('root')
);

