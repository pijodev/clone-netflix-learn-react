import React from 'react';
import { Link as ReactRouterLink } from 'react-router-dom';

import { StyledButtonLink, StyledLogo, StyledContainer, StyledBackground } from './styles/header';

export default function Header({ bg = 'true', children, ...restProps }) {

    return bg ? (
        <StyledBackground {...restProps}>
            {children}
        </StyledBackground>
    ) : { children };
}

Header.Frame = function HeaderFrame({ children, ...restProps }) {
    return (

        <StyledContainer {...restProps}>
            {children}
        </StyledContainer>
    )
}

Header.Logo = function HeaderLogo({ to, ...restProps }) {
    return (
                // vérifier combien de to=to et où 
        <ReactRouterLink to={to}>
            <StyledLogo {...restProps} to={to}/>
        </ReactRouterLink>
    )
}

Header.ButtonLink = function HeaderButtonLink({ to, children, ...restProps }) {
    return (
        // vérifier combien de to=to et où 
        <ReactRouterLink to={to}>
            <StyledButtonLink to={to} {...restProps}>
                {children}
            </StyledButtonLink>
        </ReactRouterLink>
    )
}


