import React from 'react';
import { StyledContainer, StyledSubTitle, StyledTitle } from './styles/feature';


export default function Feature({ children, ...restProps }) {
    return (
        <StyledContainer {...restProps}>{children}</StyledContainer>
    );
}


Feature.Title = function FeatureTitle({ children, ...restProps }) {
    return (
        <StyledTitle {...restProps}> {children} </StyledTitle>
    );
}

Feature.SubTitle = function FeatureSubTitle({ children, ...restProps }) {
    return (
        <StyledSubTitle {...restProps}> {children} </StyledSubTitle>
    );
}