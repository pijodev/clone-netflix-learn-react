import { StyledContainer, StyledRow, StyledColumn, StyledLink, StyledText, StyledTitle, StyledBreak} from './styles';

export default function Footer({ children, ...restProps }) {
    // Il est pratique de tout retourner dans un container pour le COMPOUND COMPONENT par défaut
    // car il y a souvent des petits sous composants à positionner dedans : Footer.Title, Footer.Columns etc..

    return <StyledContainer {...restProps}> {children} </StyledContainer>;
}



Footer.Row = function FooterRow({children, ...restProps}){
    return <StyledRow {...restProps}>{children}</StyledRow>;
}

Footer.Column = function FooterColumn({ children, ...restProps }) {
    return <StyledColumn {...restProps}>{children}</StyledColumn>;
}

Footer.Link = function FooterLink({ children, ...restProps }) {
    return <StyledLink {...restProps}>{children}</StyledLink>;
}

Footer.Title = function FooterTitle({ children, ...restProps }) {
    return <StyledTitle {...restProps}>{children}</StyledTitle>;
}

Footer.Text = function FooterText({ children, ...restProps }) {
    return <StyledText {...restProps}>{children}</StyledText>;
}

Footer.Break = function FooterBreak({...restProps }) {
    return <StyledBreak {...restProps}></StyledBreak>;
}