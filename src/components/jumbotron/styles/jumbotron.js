import styled from 'styled-components/macro';



export const StyledItem = styled.div`
    display: flex;
    border-bottom: 8px solid #222;
    padding: 50px 5%;
    color: white;
    overflow: hidden;
`;

// ${StyledItem} : le dernier <StyledItem/> à s'afficher aura une marge basse suppl. de 50px sous son h2.
// Quand on fait référence à un autre styled component, il faut que ce dernier soit déclaré avant.
export const StyledContainer = styled.div`
    @media (max-width: 100px) {
        ${StyledItem}: last-of-type h2 {
            margin-bottom: 50px;
        }
    }    
`;

// ${prop} se réfère à la valeur de la propriété issue de l'objet du DOM lui-même.
// ex : si j'ai un <input type="password"> alors ${ ({type}) => type} renvoie password.
export const StyledInner = styled.div`
    display: flex;
    flex-direction: ${ ({ direction }) => direction }; 
    align-items: center;
    justify-content: space-between;
    max-width: 1100px;
    margin: auto;
    width: 100%;


    @media (max-width: 1000px) {
        flex-direction: column
    }
`;
// @media = quand visualisé sur mobile

// 50% à gauche, 50% à droite
export const StyledPane = styled.div`
    width: 50%  

    @media( max-width: 100%) {
        width: 100%;
        padding: 0 45px;
        text-align: center;
    }
`;
export const StyledTitle = styled.h1`
font-size: 50px;
    line-height: 1.1;
    margin-bottom: 8px;

    @media(max-width: 600px) {
        font-size: 35px
    }
`;

export const StyledSubTitle = styled.h2`
    font-size: 26px;
    font-weight: normal;
    line-height: normal;

    @media(max-width: 600px) {
        font-size: 18px
    }
`;

export const StyledImage = styled.img`
    max-width: 100%;
    height: auto;
`;