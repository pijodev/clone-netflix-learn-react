import React from 'react';
import { StyledContainer, StyledInner, StyledItem, StyledPane, 
    StyledTitle, StyledSubTitle, StyledImage } from './styles/jumbotron';

// Jumbotron est un COMPOUND component. Dans ce tuto, on veut laisser la possibilité de donner 
// n'importe quelle prop au jumbo, alors on note ...restProps pour les prendre en compte.
// Donner également le children en 1er. Children est ce qui est affiché dans le composant JSX.

// On donne une direction dir par défaut au jumbotron, elle est écrasée si le parent transmet sa direction
export default function Jumbotron({ children, dir='row', ...restProps }) {
    // Inner est une balise stylisée perso
    // Item sert à créer des modifications de style (comme les bordures) sur le Inner sans modifier ce dernier
    return (
        <StyledItem {...restProps}>
            <StyledInner direction={dir}>
                {/* Children c'est chaque élément enfant donné dans la balise <Jumbotron> */}
                {children}
            </StyledInner>
        </StyledItem>


    );
}

// Container est un COMPOUND COMPONENT. Ce sont des composants qui partagent un même état ou logique.
// Il affiche le contenu de son enfant. 
// L'intérêt c'est que ça fait un genre d'arbre de composants dans un seul composant. C'est asssez joli

// Children de Container = Jumbotron qui lui même a pour enfant un StyledInner
Jumbotron.Container = function JumbotronContainer({ children, ...restProps }) {
    return <StyledContainer {...restProps}> {children} </StyledContainer>;
}

Jumbotron.Pane = function JumbotronPane({ children, ...restProps }) {
    return <StyledPane {...restProps}> {children} </StyledPane>;
}

Jumbotron.Title = function JumbotronTitle({ children, ...restProps }) {
    return <StyledTitle {...restProps}> {children} </StyledTitle>;
}

Jumbotron.SubTitle = function JumbotronSubTitle({ children, ...restProps }) {
    return <StyledSubTitle {...restProps}> {children} </StyledSubTitle>;
}

Jumbotron.Image = function JumbotronImage({ ...restProps }) {
    return <StyledImage {...restProps} />;
}