import React, { createContext, useContext, useState } from 'react';
import { StyledContainer, Inner, StyledItem, StyledHeader, StyledFrame, StyledTitle, StyledBody} from './styles/accordion';


// Permet de passer le state du toggle de l'Item vers le Header
// Toute fonction appelant useContext() peut désormais consommer l'état via le Contexte.
const ToggleContext = createContext();

export default function Accordion({ children, ...restProps }) {

    return (
        <StyledContainer {...restProps}>
            <Inner>{children}</Inner>
        </StyledContainer>
    )
}

// Contient la question, la réponse, le toggle pour déployer la question.
// L'Item reçoit tous les changements d'états c'est pour ça que c'est lui le conteneur principal
// Il va pouvoir donner la possibilité de toggle ou non
// Item consomme l'état également.
Accordion.Item = function AccordionItem({ children, ...restProps }) { // Primary Container
    const [toggleShow, setToggleShow] = useState(false); 

    // Le xyzContext.Provider est le pont qui permet de passer l'état du toggle via le contexte au Header.
    // Mais comment lui passer ? grâce useContext(ToggleContext); qui sera dans le  Header.
    return (
        <ToggleContext.Provider value={{toggleShow, setToggleShow}}>
            <StyledItem {...restProps}>{children}</StyledItem>
        </ToggleContext.Provider>
    )
}

// Quand on clique sur la question du Header ou sur le bouton, le toggle change d'état.
// L'Item doit savoir si sa question a été cliquée.
// Justement, Header sera un child d'Item. Donc il sera wrap par Item. Donc il va recevoir l'état.
Accordion.Header = function AccordionHeader({ children, ...restProps }) {
    const {toggleShow, setToggleShow} = useContext(ToggleContext);
    // Au clic sur le header, on change l'état
    // Pourquoi : setToggleShow((toggleShow => !toggleShow)) et pas setToggleShow(!toggleShow)
    // Parfois React lorsque les variables s'accumulent, va relâcher ce qui n'est plus utilisé.
    // Ex: si le header est déployé, que React relâche, et qu'il est codé setToggleShow(!toggleShow)
    // Alors si on veut détoggler, il ne se fermera pas.
    return (
    <StyledHeader onClick={() => setToggleShow((toggleShow => !toggleShow))} {...restProps}>
        {children}
        { toggleShow ? 
                (<img src="/images/icons/close-slim.png" alt="Close" />) : 
                (<img src="/images/icons/add.png" alt="Open" />)}
    </StyledHeader>
    )
}




Accordion.Body = function AccordionBody({ children, ...restProps }) {
    const { toggleShow } = useContext(ToggleContext);
    console.log("CONTEXT : " + useContext(ToggleContext));
    // si = vrai alors on affiche le contenu sinon rien
    return toggleShow ? <StyledBody {...restProps}>{children}</StyledBody> : null;
}


Accordion.Title = function AccordionTitle({ children, ...restProps }) {
    return <StyledTitle {...restProps}>{children}</StyledTitle>
}

Accordion.Frame = function AccordionFrame({ children, ...restProps }) {
    return <StyledFrame {...restProps}>{children}</StyledFrame>
}





