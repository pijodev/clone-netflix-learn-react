export { default as Jumbotron } from './jumbotron';

// cette ligne permet d'exporter 
// export default Jumbotron dans : 
// src/components/jumbotron/index.js 
// par défaut, et on peut l'importer avec les { }

// Ce fichier index.js est la REFERENCE pour tous les composants que l'on va créer.

// C'est pratique pour faire attention à ce qu'on met dans les bundle quand on construit le webpack.

export { default as Footer } from './footer';
export { default as Accordion } from './accordion';
export { default as OptForm } from './opt-form';
export { default as Header } from './header';
export { default as Feature } from './feature';
export { default as Form } from './form';
