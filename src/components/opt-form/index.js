import { StyledContainer, StyledInput, StyledButton, StyledText} from "./styles/opt-form.js";

export default function OptForm({children, ...restProps}){
    return (
        <StyledContainer {...restProps}>{children}</StyledContainer>
    )
}

// Un input a rarement un enfant, mais un bouton si.
OptForm.Input = function OptFormInput({...restProps}){
    return <StyledInput {...restProps}></StyledInput>
}

OptForm.Button = function OptFormButton({ children, ...restProps }) {
    return <StyledButton {...restProps}>
        {children}
        <img src="/images/icons/chevron-right.png"/>
    </StyledButton>
}

// Texte du bouton ?
OptForm.Text = function OptFormText({ children, ...restProps }) {
    return <StyledText {...restProps}>
        {children}
    </StyledText>
}

// OptForm.Footer = function OptFormFooter({ children, ...restProps }) {
//     return <StyledFooter {...restProps}>
//         {children}
//     </StyledFooter>
// }