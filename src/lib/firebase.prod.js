import Firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
// import { seedDatabase } from '../seed';

// DB

// CONFIG
const config = {
    apiKey: "AIzaSyC_Tw_QKgmdsW1dZQWtPVAXOFgrrSofsZY",
    authDomain: "netflix-clone-2678b.firebaseapp.com",
    projectId: "netflix-clone-2678b",
    storageBucket: "netflix-clone-2678b.appspot.com",
    messagingSenderId: "63016311050",
    appId: "1:63016311050:web:27eed2ea5efffb61d102f3"
};

const firebase = Firebase.initializeApp(config);

// ATTENTION : 
// à chaque fois que firebase.prod.js est importé ou raffraichi, ça va déclencher l'appel seedDatabase(config)
// CAD : ça va peupler les collections dans le firestore dédié à l'application.
// Si c'est fait plus de 1 fois, toutes les données seront dupliquées, on ne veut pas que ça arrive.
// Donc, on commentera cette ligne ci-dessous et on ne l'appelera que la 1ère fois ou après avoir vidé firestore.

// ligne supprimée : seedDatabase(firebase);

export { firebase };