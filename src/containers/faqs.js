import { Accordion, OptForm } from '../components';

import faqsData from '../fixtures/faqs.json';

// Concernant le placeholder du OptForm.Input, c'est parfait car Accordion passe les restProps
// à ses enfants. Et comme Input est l'un de ses enfants, c'est peut-être utile pour transférer l'info ?

export function FaqsContainer() {
    return (

        <Accordion>
            <Accordion.Title>Questions fréquentes</Accordion.Title>

            {faqsData.map((item) => (
                <Accordion.Item key={item.id}>
                    <Accordion.Header>{item.header}</Accordion.Header>
                    <Accordion.Body>{item.body}</Accordion.Body>
                </Accordion.Item>
            ))}

            <OptForm>
                <OptForm.Input placeholder="Votre email" />
                <OptForm.Button > Essayez Netflix ! </OptForm.Button>
                <OptForm.Text > Bla bla bla rejoignez nous 😇 </OptForm.Text>

            </OptForm>

        </Accordion>



    )
}