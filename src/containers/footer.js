import { Footer } from '../components';

export function FooterContainer({ children, ...restProps }) {

    return (
        <Footer>
            <Footer.Title>Des questions ? Appelez le 0000-111-222</Footer.Title>
            <Footer.Break></Footer.Break>

            <Footer.Row>
                <Footer.Column>
                    <Footer.Link href="#">FAQ</Footer.Link>
                    <Footer.Link href="#">Investor Relations</Footer.Link>
                    <Footer.Link href="#">Modes de Lecture</Footer.Link>
                    <Footer.Link href="#">Mentions Légales</Footer.Link>
                    <Footer.Link href="#">Netflix Originals</Footer.Link>
                </Footer.Column>

                <Footer.Column>
                    <Footer.Link href="#">Centre d'aide</Footer.Link>
                    <Footer.Link href="#">Recrutement</Footer.Link>
                    <Footer.Link href="#">Condition d'Utilisation</Footer.Link>
                    <Footer.Link href="#">Contact</Footer.Link>
                </Footer.Column>

                <Footer.Column>
                    <Footer.Link href="#">Compte</Footer.Link>
                    <Footer.Link href="#">Cartes cadeaux</Footer.Link>
                    <Footer.Link href="#">Confidentialité</Footer.Link>
                    <Footer.Link href="#">Speed Test</Footer.Link>
                </Footer.Column>

                <Footer.Column>
                    <Footer.Link href="#">Presse</Footer.Link>
                    <Footer.Link href="#">Acheter une carte cadeau</Footer.Link>
                    <Footer.Link href="#">Cookies</Footer.Link>
                    <Footer.Link href="#">Infos légales</Footer.Link>
                </Footer.Column>
            </Footer.Row>

            <Footer.Break/>
            <Footer.Text>Netflix France</Footer.Text>
        </Footer>

    )
}