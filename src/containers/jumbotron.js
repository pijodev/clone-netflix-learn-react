import jumboData from '../fixtures/jumbo.json';
import { Jumbotron } from '../components/'; // renvoie à src/components/jumbotron/index.js

// Pas un default car
export function JumbotronContainer() {
    return (
        <div>
            {/* <p>
        Ca part de là :
    </p> */}

            {/* Jumbotron Container est un genre de bac dans lequel on va créer autant de Jumbos qu'il y a d'éléments
dans le jumbo.json. */}

            <Jumbotron.Container>
                {jumboData.map(
                    (item) => (
                        <Jumbotron key={item.id} direction={item.direction}>
                            {/* Pane est un compound pour éviter de s'embêter avec du flex directement */}
                            <Jumbotron.Pane>
                                <Jumbotron.Title>{item.title}</Jumbotron.Title>
                                <Jumbotron.SubTitle>{item.subTitle}</Jumbotron.SubTitle>
                            </Jumbotron.Pane>

                            <Jumbotron.Pane>
                                <Jumbotron.Image src={item.image} alt={item.alt} />
                            </Jumbotron.Pane>
                        </Jumbotron>
                    )
                )}
            </Jumbotron.Container>
        </div>

    );
}

