Pourquoi ce dossier container ? 

En fait on a vu qu'au début, le jumbotron était directement injecté dans le app.js

Mais on veut un app.js clean, pas trop dense.

Donc on crée un dossier containers dans lequel on va faire toute la mise en page et ensuite on appelera le container dans le app.js.

